<?php
/**
 * @file
 * jirgensons_drupal_prd3.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function jirgensons_drupal_prd3_taxonomy_default_vocabularies() {
  return array(
    'produkti' => array(
      'name' => 'Produkti',
      'machine_name' => 'produkti',
      'description' => 'Šajā vārdnīcā būs produkti',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
