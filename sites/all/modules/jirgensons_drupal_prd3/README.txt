﻿Tikai izveidoti seši raksti, kā prasīts uzdevuma noteikumos.
Katrs raksts ir kāda konkrēta produkta apskats, kam pievienota
bilde, apraksts, avots (aprakstā) un nosaukums, taču pārējie lauki
ģenerēti automātiski.