<?php
/**
 * @file
 * jirgensons_drupal_prd.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function jirgensons_drupal_prd_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
