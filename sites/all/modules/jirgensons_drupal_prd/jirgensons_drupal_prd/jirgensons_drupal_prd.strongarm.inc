<?php
/**
 * @file
 * jirgensons_drupal_prd.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function jirgensons_drupal_prd_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_mail';
  $strongarm->value = 'jirgic@hotmail.com';
  $export['site_mail'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_name';
  $strongarm->value = 'Jirgensons Drupal PrD';
  $export['site_name'] = $strongarm;

  return $export;
}
