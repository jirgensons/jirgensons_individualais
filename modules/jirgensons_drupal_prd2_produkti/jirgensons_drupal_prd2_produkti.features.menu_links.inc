<?php
/**
 * @file
 * jirgensons_drupal_prd2_produkti.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function jirgensons_drupal_prd2_produkti_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_produkts:node/add/produkts
  $menu_links['navigation_produkts:node/add/produkts'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/produkts',
    'router_path' => 'node/add/produkts',
    'link_title' => 'Produkts',
    'options' => array(
      'attributes' => array(
        'title' => 'Satura tips produktiem',
      ),
      'identifier' => 'navigation_produkts:node/add/produkts',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Produkts');


  return $menu_links;
}
